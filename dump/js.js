createOffsets();
window.addEventListener("resize", createOffsets);
 
function createOffsets() {
  const sectionWithLeftOffset = document.querySelector(
    ".section-with-left-offset"
  );
  const sectionWithLeftOffsetCarouselWrapper = sectionWithLeftOffset.querySelector(
    ".carousel-wrapper"
  );
  const sectionWithRightOffset = document.querySelector(
    ".section-with-right-offset"
  );
  const sectionWithRightOffsetCarouselWrapper = sectionWithRightOffset.querySelector(
    ".carousel-wrapper"
  );
  const offset = (window.innerWidth - 1100) / 2;
  const mqLarge = window.matchMedia("(min-width: 1200px)");
 
  if (sectionWithLeftOffset && mqLarge.matches) {
    sectionWithLeftOffsetCarouselWrapper.style.marginLeft = offset + "px";
  } else {
    sectionWithLeftOffsetCarouselWrapper.style.marginLeft = 0;
  }
 
  if (sectionWithRightOffset && mqLarge.matches) {
    sectionWithRightOffsetCarouselWrapper.style.marginRight = offset + "px";
  } else {
    sectionWithRightOffsetCarouselWrapper.style.marginRight = 0;
  }
}